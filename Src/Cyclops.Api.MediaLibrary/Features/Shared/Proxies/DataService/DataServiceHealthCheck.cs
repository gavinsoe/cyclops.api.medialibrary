﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Infrastructure.HealthChecks;

namespace Cyclops.Api.MediaLibrary.Features.Shared.Proxies.DataService
{
    [AutoBind]
    public class DataServiceHealthChecker : IHealthChecker
    {
        private readonly IDataServiceApiProxy _proxy;

        public DataServiceHealthChecker(IDataServiceApiProxy proxy)
        {
            _proxy = proxy;
        }

        public async Task<IHealthCheckResult> CheckAsync()
        {
            var result = await _proxy.GetAsync<dynamic>(new DataServiceInput
            {
                NetworkIds = new []{"OAG-AD-12727045","SSE-AD-4142029"}
            });

            return new HealthCheckResult
            {
                IsHealthy = (result.Succeed || result.StatusCode == HttpStatusCode.NotFound),
                FailureSeverity = FailureSeverity.Fatal,
                Name = "DataServiceApi",
                Details = result.Result
            };
        }
    }
}