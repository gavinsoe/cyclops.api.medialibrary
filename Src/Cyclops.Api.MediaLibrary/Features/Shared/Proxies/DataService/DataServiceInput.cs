using System.Collections.Generic;

namespace Cyclops.Api.MediaLibrary.Features.Shared.Proxies.DataService
{
    public class DataServiceInput
    {
        public IEnumerable<string> NetworkIds { get; set; }
        public string Projection { get; set; }
    }
}