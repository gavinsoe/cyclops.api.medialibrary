﻿using System.Threading.Tasks;
using Csn.Hystrix.RestClient;
using Csn.Hystrix.RestClient.Dtos;
using Csn.Lifestyle.Common.Extensions;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;

namespace Cyclops.Api.MediaLibrary.Features.Shared.Proxies.DataService
{
    public interface IDataServiceApiProxy
    {
        Task<HystrixRestResponse<T>> GetAsync<T>(DataServiceInput input);
    }

    [AutoBind]
    public class DataServiceApiProxy : IDataServiceApiProxy
    {
        private const string HostName = "DataServiceApiProxy";
        private readonly IFluentHystrixRestClientFactory _restClient;

        public DataServiceApiProxy(IFluentHystrixRestClientFactory restClient)
        {
            _restClient = restClient;
        }

        public Task<HystrixRestResponse<T>> GetAsync<T>(DataServiceInput input)
        {
            return _restClient.HostName(HostName)
                                    .Path("items")
                                    .QueryParams("ids", string.Join(",", input.NetworkIds))
                                    .QueryParams("projection", input.Projection.HasValue() ? input.Projection : null)
                                    .GetAsync<T>();
        }
    }
}