﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cyclops.Api.MediaLibrary.Features.Shared
{
    public static class ImageCategory
    {
        public static string BadgeExterior => "badge exterior";
        public static string BootInterior => "boot interior";
        public static string BuildAndCompliance => "build and compliance";
        public static string Dash => "dash";
        public static string DoorHandleExterior => "door handle exterior";
        public static string DoorHandleInterior => "door handle interior";
        public static string Engine => "engine";
        public static string Front34 => "front 34";
        public static string Front34Driver => "front 34 driver";
        public static string Front34Passenger => "front 34 passenger";
        public static string FrontFullFront => "front full front";
        public static string Gearshift => "gearshift";
        public static string GpsInfotainment => "gps infotainment";
        public static string Headlight => "headlight";
        public static string HighlightExterior => "highlight exterior";
        public static string HighlightInterior => "highlight interior";
        public static string InteriorFront => "interior front";
        public static string InteriorRear => "interior rear";
        public static string Misc => "misc";
        public static string Rear34 => "rear 34";
        public static string Rear34Driver => "rear 34 driver";
        public static string Rear34Passenger => "rear 34 passenger";
        public static string RearFullRear => "rear full rear";
        public static string Seat => "seat";
        public static string Side34 => "side 34";
        public static string SideMirror => "side mirror";
        public static string SideView => "side view";
        public static string SpeedoCluster => "speedo cluster";
        public static string SteeringWheel => "steering wheel";
        public static string SunroofInterior => "sunroof interior";
        public static string TailLight => "tail light";
        public static string Wheel => "wheel";
    }
}