﻿using System.Threading.Tasks;
using System.Web.Http;
using Cyclops.Api.MediaLibrary.Infrastructure.HealthChecks;

namespace Cyclops.Api.MediaLibrary.Features.Shared.HealthCheck
{
    public class HealthCheckController : ApiController
    {
        private readonly IHealthReporter _reporter;

        public HealthCheckController(IHealthReporter reporter)
        {
            _reporter = reporter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("v1/healthcheck")]
        public async Task<IHttpActionResult> HealthCheck()
        {
            return Ok(await _reporter.ReportAsync());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("v1/ping")]
        public IHttpActionResult Ping()
        {
            return Ok();
        }
    }
}