﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cyclops.Api.MediaLibrary.Features.Shared
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ImageCroppingType
    {
        NotSpecified,
        Left,
        Right
    }
}