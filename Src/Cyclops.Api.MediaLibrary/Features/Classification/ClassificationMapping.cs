﻿using Cyclops.Api.MediaLibrary.Infrastructure.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Cyclops.Api.MediaLibrary.Features.Classification.Dtos;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;

namespace Cyclops.Api.MediaLibrary.Features.Classification
{
    [AutoBind]
    public class ClassificationMapping : IMappingSetupTask
    {
        public void Run(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ClassificationRequest, CyclopsApiRequest>()
                .ForMember(d => d.Images, opt => opt.MapFrom(src => src.Images));

            cfg.CreateMap<CyclopsApiResponse, ClassificationResponse>()
                .ForMember(d => d.ClassificationResults, opt => opt.MapFrom(src => src.ClassificationResults.GetFinalClassifications()))
                .ForMember(d => d.SuccessCount, opt => opt.MapFrom(src => src.SuccessCount));
        }
    }

    public static class ClassificationExtensions
    {
        public static List<FinalClassificationDto> GetFinalClassifications(this List<ClassificationResultDto> classifications)
        {
            var result = new List<FinalClassificationDto>();

            classifications.ForEach(x =>
            {
                var c = new FinalClassificationDto();
                c.Image = x.Image;
                x.Categories.OrderByDescending(y => y.ConfidenceScore);
                c.Category1 = x.Categories.Take(1)?.FirstOrDefault();
                c.Category2 = x.Categories.Skip(1).Take(1)?.FirstOrDefault();

                result.Add(c);
            });

            return result;
        }
    }
}