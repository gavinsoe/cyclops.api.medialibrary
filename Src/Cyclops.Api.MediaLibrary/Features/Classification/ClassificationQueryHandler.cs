﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cyclops.Api.MediaLibrary.Features.Classification.Dtos;
using Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.FirstPass;
using Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.SecondPass;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Infrastructure.Mappers;
using Csn.SimpleCqrs;

namespace Cyclops.Api.MediaLibrary.Features.Classification
{
    public class ClassificationRequest : IQuery
    {
        public ClassificationRequest()
        {
            Images = new List<ImageDto>();
        }
        public List<ImageDto> Images { get; set; }
    }

    public class ClassificationResponse
    {
        public ClassificationResponse()
        {
            ClassificationResults = new List<FinalClassificationDto>();
        }
        public List<FinalClassificationDto> ClassificationResults { get; set; }
        public int SuccessCount { get; set; }
    }

    [AutoBind]
    public class ClassificationQueryHandler : IAsyncQueryHandler<ClassificationRequest, ClassificationResponse>
    {
        private readonly IEnumerable<IFirstPass<CyclopsApiRequest, CyclopsApiResponse>> _firstPass;
        private readonly IEnumerable<ISecondPass<CyclopsApiRequest, CyclopsApiResponse>> _secondPass;
        private readonly IMapper _mapper;

        public ClassificationQueryHandler(IEnumerable<IFirstPass<CyclopsApiRequest, CyclopsApiResponse>> firstPass,
                                          IEnumerable<ISecondPass<CyclopsApiRequest, CyclopsApiResponse>> secondPass,
                                          IMapper mapper)
        {
            _firstPass = firstPass;
            _secondPass = secondPass;
            _mapper = mapper;
        }

        public async Task<ClassificationResponse> HandleAsync(ClassificationRequest request)
        {
            var apiRequest = _mapper.Map<CyclopsApiRequest>(request);
            var apiResponse = new CyclopsApiResponse();
            
            // Classification first pass
            await Task.WhenAll(_firstPass.Select(x => x.ClassifyAsync(apiRequest, apiResponse)));

            // Classifications second pass
            await Task.WhenAll(_secondPass.Select(x => x.ClassifyAsync(apiRequest, apiResponse)));

            return _mapper.Map<ClassificationResponse>(apiResponse);
        }
    }
}