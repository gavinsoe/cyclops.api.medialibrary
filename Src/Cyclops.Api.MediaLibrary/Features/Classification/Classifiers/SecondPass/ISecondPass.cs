﻿using System.Threading.Tasks;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.SecondPass
{
    public interface ISecondPass<in TRequest, in TResponse>
    {
        Task ClassifyAsync(TRequest request, TResponse response);
    }
}