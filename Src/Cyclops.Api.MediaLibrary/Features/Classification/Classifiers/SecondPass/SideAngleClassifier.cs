﻿using System.Linq;
using System.Threading.Tasks;
using Csn.Hystrix.RestClient;
using Cyclops.Api.MediaLibrary.Features.Shared;
using Cyclops.Api.MediaLibrary.Features.Classification.Dtos;
using System.Collections.Generic;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.SecondPass
{
    [AutoBind]
    public class SideAngleClassifier : ISecondPass<CyclopsApiRequest, CyclopsApiResponse>
    {
        private const string Host = "CyclopsApiProxy";
        private readonly IFluentHystrixRestClientFactory _restClient;

        public SideAngleClassifier(IFluentHystrixRestClientFactory restClient)
        {
            _restClient = restClient;
        }

        public async Task ClassifyAsync(CyclopsApiRequest request, CyclopsApiResponse response)
        {
            // pass through images that are classified as SideView
            var newRequest = new CyclopsApiRequest();
        
            // find images to process
            var images = response.ClassificationResults.Where(x =>
            {
                var category = x.Categories.OrderByDescending(y => y.ConfidenceScore).First().Name;
                if (category.Equals(ImageCategory.Front34) ||
                    category.Equals(ImageCategory.Rear34))
                {
                    return true;
                }
                return false;
            }).ToList();

            // add images to process
            newRequest.Images.AddRange(images.Select(x =>
            {
                x.Image.CroppingType = ImageCroppingType.Left;
                return x.Image;
            }));
            newRequest.Images.AddRange(images.Select(x =>
            {
                x.Image.CroppingType = ImageCroppingType.Right;
                return x.Image;
            }));


            // temp for testing
            var result = await _restClient.HostName(Host)
                .Path("/Data/api-response-angle.json")
                .ContentTypeJson()
                .GetAsync<CyclopsApiResponse>();

            // process
            /*var result = await _restClient.HostName(Host)
                .Path("/side") // {ClassificationMode}/{Graph}                
                .ContentTypeJson()
                .PostAsync<CyclopsApiRequest, CyclopsApiResponse>(newRequest);*/

            var processedResult = PostProcessing(response, result.Result);
            response.ClassificationResults = processedResult.ClassificationResults;
            response.SuccessCount = processedResult.SuccessCount;
            //response = PostProcessing(response, result.Result);
        }

        private CyclopsApiResponse PostProcessing(CyclopsApiResponse original, CyclopsApiResponse updated)
        {
            var leftResults = updated.ClassificationResults.Where(x => x.Image.CroppingType == ImageCroppingType.Left).ToList();
            var rightResults = updated.ClassificationResults.Where(x => x.Image.CroppingType == ImageCroppingType.Right).ToList();

            leftResults.ForEach(left =>
            {
                var imgUrl = left.Image.ImageUrl;
                var right = rightResults.FirstOrDefault(y => y.Image.ImageUrl == imgUrl);
                if (right != null)
                {
                    original.ClassificationResults.Find(i => i.Image.ImageUrl == imgUrl).Categories = ResolveCategory(left, right);                     
                }
            });

            return original;
        }

        private List<CategoryDto> ResolveCategory(ClassificationResultDto leftImage, ClassificationResultDto rightImage)
        {
            leftImage.Categories.OrderByDescending(x => x.ConfidenceScore);
            CategoryDto leftCat1 = leftImage.Categories.Take(1).FirstOrDefault();
            CategoryDto leftCat2 = leftImage.Categories.Skip(1).Take(1).FirstOrDefault();
            
            rightImage.Categories.OrderByDescending(x => x.ConfidenceScore);
            CategoryDto rightCat1 = rightImage.Categories.Take(1).FirstOrDefault();
            CategoryDto rightCat2 = rightImage.Categories.Skip(1).Take(1).FirstOrDefault();

            var selectedCategory = ResolveCategory(leftCat1, rightCat1, leftCat2,rightCat2);
            var updatedClassification = new ClassificationResultDto();

            updatedClassification.Categories.Add(new CategoryDto { Name = selectedCategory });
            updatedClassification.Image = leftImage.Image;
            updatedClassification.Image.CroppingType = ImageCroppingType.NotSpecified;

            return updatedClassification.Categories;
        }

        private string ResolveCategory(CategoryDto left1, CategoryDto right1,
                                       CategoryDto left2, CategoryDto right2)
        {
            // if one of the category(1) is a side
            if (left1.Name.Equals(ImageCategory.Side34) != right1.Name.Equals(ImageCategory.Side34))
            {
                return PickWinningCategory(left1, right1);
            }
            else
            {
                if (left1.Name == right1.Name)
                {
                    // if a combination of category(1) and category(2) has a side
                    if ((left1.Name.Equals(ImageCategory.Side34) != right2.Name.Equals(ImageCategory.Side34)) &&
                        (left2.Name.Equals(ImageCategory.Side34) != right1.Name.Equals(ImageCategory.Side34)))
                    {
                        if (left2.ConfidenceScore > right2.ConfidenceScore)
                        {
                            return PickWinningCategory(left2, right1);
                        }
                        else
                        {
                            return PickWinningCategory(left1, right2);
                        }
                    }
                    else if (left1.Name.Equals(ImageCategory.Side34) != right2.Name.Equals(ImageCategory.Side34))
                    {
                        return PickWinningCategory(left1, right2);
                    }
                    else if (left2.Name.Equals(ImageCategory.Side34) != right1.Name.Equals(ImageCategory.Side34))
                    {
                        return PickWinningCategory(left2, right1);
                    }
                }
                else
                {
                    // if both of the second category is a side
                    if (left2.Name.Equals(ImageCategory.Side34) && right2.Name.Equals(ImageCategory.Side34))
                    {
                        // use category(1) when category(2) is lower than the other
                        if (left2.ConfidenceScore > right2.ConfidenceScore)
                        {
                            return PickWinningCategory(left2, right1);
                        }
                        else
                        {
                            return PickWinningCategory(left1, right2);
                        }
                    }
                    // if only the left category(2) is a side
                    else if (left2.Name.Equals(ImageCategory.Side34))
                    {
                        return PickWinningCategory(left2, right1);
                    }
                    // if only the right category(2) is a side
                    else if (right2.Name.Equals(ImageCategory.Side34))
                    {
                        return PickWinningCategory(left1, right2);
                    }
                }
            }

            // Default response when unable to resolve.
            return ImageCategory.Misc;
        }

        /// <summary>
        /// Picks final category
        /// </summary>
        /// <param name="cat1"></param>
        /// <param name="cat2"></param>
        /// <returns></returns>
        private string PickWinningCategory(CategoryDto cat1, CategoryDto cat2)
        {
            if (cat1.Name.Equals(ImageCategory.Side34))
            {
                if (cat2.Name.Equals(ImageCategory.Front34))
                    return ImageCategory.Front34Driver;
                if (cat2.Name.Equals(ImageCategory.Rear34))
                    return ImageCategory.Rear34Passenger;
            }
            else if (cat2.Name.Equals(ImageCategory.Side34))
            {
                if (cat1.Name.Equals(ImageCategory.Front34))
                    return ImageCategory.Front34Passenger;
                if (cat1.Name.Equals(ImageCategory.Rear34))
                    return ImageCategory.Rear34Driver;
            }

            // shouldn't really end up here, but if it does... return misc
            return ImageCategory.Misc;
        }
        
    }
}