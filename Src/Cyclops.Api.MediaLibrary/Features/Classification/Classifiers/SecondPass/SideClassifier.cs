﻿using System.Threading.Tasks;
using Csn.Hystrix.RestClient;
using System.Linq;
using Cyclops.Api.MediaLibrary.Features.Classification.Dtos;
using Cyclops.Api.MediaLibrary.Features.Shared;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.SecondPass
{
    [AutoBind]
    public class SideClassifier : ISecondPass<CyclopsApiRequest, CyclopsApiResponse>
    {
        private const string Host = "CyclopsApiProxy";
        private readonly IFluentHystrixRestClientFactory _restClient;

        public SideClassifier(IFluentHystrixRestClientFactory restClient)
        {
            _restClient = restClient;
        }

        public async Task ClassifyAsync(CyclopsApiRequest request, CyclopsApiResponse response)
        {
            var newRequest = new CyclopsApiRequest();
            newRequest.Images = response.ClassificationResults.Where(x =>
                {
                    var category = x.Categories.OrderByDescending(y => y.ConfidenceScore).First().Name;
                    if (category == ImageCategory.SideView)
                    {
                        return true;
                    }
                    return false;
                }).Select(z => z.Image).ToList();

            // temp for testing
            var result = await _restClient.HostName(Host)
                .Path("/Data/api-response-side.json")
                .ContentTypeJson()
                .GetAsync<CyclopsApiResponse>();
            
            // process
            /*var result = await _restClient.HostName(Host)
                .Path("/side") // /{Graph}                
                .ContentTypeJson()
                .PostAsync<CyclopsApiRequest, CyclopsApiResponse>(newRequest);*/

            var processedResult = PostProcessing(response, result.Result);
            response.ClassificationResults = processedResult.ClassificationResults;
            response.SuccessCount = processedResult.SuccessCount;
            //response = PostProcessing(response, result.Result);
        }

        private CyclopsApiResponse PostProcessing(CyclopsApiResponse original, CyclopsApiResponse updated)
        {
            updated.ClassificationResults.ForEach(x =>
            {
                original.ClassificationResults.Find(i => i.Image.ImageUrl == x.Image.ImageUrl).Categories = x.Categories;
            });

            return original;
        }
    }
}