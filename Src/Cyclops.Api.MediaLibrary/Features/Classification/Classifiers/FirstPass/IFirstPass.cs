﻿using System.Threading.Tasks;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.FirstPass
{
    public interface IFirstPass<in TRequest, in TResponse>
    {
        Task ClassifyAsync(TRequest request, TResponse response);
    }
}