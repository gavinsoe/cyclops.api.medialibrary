﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Csn.Hystrix.RestClient;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Features.Classification.Dtos;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Classifiers.FirstPass
{
    [AutoBind]
    public class BasicClassifier : IFirstPass<CyclopsApiRequest, CyclopsApiResponse>
    {
        private const string Host = "CyclopsApiProxy";
        private readonly IFluentHystrixRestClientFactory _restClient;
        
        public BasicClassifier(IFluentHystrixRestClientFactory restClient)
        {
            _restClient = restClient;
        }
        
        public async Task ClassifyAsync(CyclopsApiRequest request, CyclopsApiResponse response)
        {
            // temp for testing
            var result = await _restClient.HostName(Host)
                .Path("/Data/api-response-first.json")
                .ContentTypeJson()
                .GetAsync<CyclopsApiResponse>();

            /*var result = await _restClient.HostName(Host)
                .Path("/full/basic")
                .ContentTypeJson()
                .PostAsync<CyclopsApiRequest, CyclopsApiResponse>(request);*/

            response.ClassificationResults = result.Result.ClassificationResults;
            response.SuccessCount = result.Result.SuccessCount;
        }
    }
}