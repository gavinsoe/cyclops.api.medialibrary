﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Csn.SimpleCqrs;

namespace Cyclops.Api.MediaLibrary.Features.Classification
{
    public class ClassificationController : ApiController
    {
        private readonly IQueryDispatcher _queryDispatcher;

        public ClassificationController(IQueryDispatcher queryDispatcher)
        {
            _queryDispatcher = queryDispatcher;
        }

        [Route("v1/classify/")]
        public async Task<IHttpActionResult> ClassifyImages([FromBody]ClassificationRequest query)
        {
            var vm = await _queryDispatcher.DispatchAsync<ClassificationRequest, ClassificationResponse>(query);

            return Ok(vm);
        }
    }
}