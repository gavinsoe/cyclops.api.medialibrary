﻿using Csn.SimpleCqrs;
using System.Collections.Generic;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class CyclopsApiRequest : IQuery
    {
        public CyclopsApiRequest()
        {
            Images = new List<ImageDto>();
        }
        public List<ImageDto> Images { get; set; }
    }
}