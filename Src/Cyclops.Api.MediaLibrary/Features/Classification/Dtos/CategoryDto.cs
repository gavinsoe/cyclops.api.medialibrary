﻿using Cyclops.Api.MediaLibrary.Features.Shared;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class CategoryDto
    {
        public string Name { get; set; }
        public float ConfidenceScore { get; set; }
    }
}