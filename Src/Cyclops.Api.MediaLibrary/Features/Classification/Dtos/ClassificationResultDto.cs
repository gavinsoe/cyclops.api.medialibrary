﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Cyclops.Api.MediaLibrary.Features.Shared;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class ClassificationResultDto
    {
        public ClassificationResultDto()
        {
            // set defaults
            Categories = new List<CategoryDto>();
        }

        public ImageDto Image { get; set; }
        public List<CategoryDto> Categories { get; set; }
        public string Status { get; set; }
    }
}