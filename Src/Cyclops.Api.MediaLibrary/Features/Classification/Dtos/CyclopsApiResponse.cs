﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class CyclopsApiResponse
    {
        public CyclopsApiResponse()
        {
            ClassificationResults = new List<ClassificationResultDto>();
        }
        public List<ClassificationResultDto> ClassificationResults { get; set; }
        public int SuccessCount { get; set; }
    }
}