﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class FinalClassificationDto
    {
        public ImageDto Image { get; set; }
        public CategoryDto Category1 { get; set; }
        public CategoryDto Category2 { get; set; }
    }
}