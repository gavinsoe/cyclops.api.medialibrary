﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cyclops.Api.MediaLibrary.Features.Shared;

namespace Cyclops.Api.MediaLibrary.Features.Classification.Dtos
{
    public class ImageDto
    {
        public string ImageUrl { get; set; }
        public ImageCroppingType CroppingType { get; set; }
        public ImageSourceType SourceType { get; set; }
    }
}