﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Cors;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Cyclops.Api.MediaLibrary;
using Cyclops.Api.MediaLibrary.Infrastructure.StartUpTasks;
using Csn.SimpleCqrs.Extended;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Cyclops.Api.MediaLibrary
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = GlobalConfiguration.Configuration;

            var builder = DependencySetup.Run(config);

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            RunStartUpTasks(container);

        }

        private static void RunStartUpTasks(IContainer container)
        {
            var tasks = container.Resolve<IEnumerable<IStartUpTask>>() ?? Enumerable.Empty<IStartUpTask>();
            foreach (var startUpTask in tasks)
            {
                startUpTask.Run();
            }
        }
    }

    public static class DependencySetup
    {
        public static ContainerBuilder Run(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            var webAssembly = typeof(Startup).Assembly;
            builder.RegisterApiControllers(webAssembly);
            builder.RegisterAssemblyModules(webAssembly, typeof(Csn.SimpleCqrs.AutoFac.DependencyResolver).Assembly);
            builder.RegisterWebApiFilterProvider(config);

            return builder;
        }
    }
}
