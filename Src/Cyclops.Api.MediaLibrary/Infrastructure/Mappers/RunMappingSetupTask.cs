﻿using System.Collections.Generic;
using AutoMapper;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Infrastructure.StartUpTasks;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Mappers
{
    [AutoBind]
    public class RunMappingSetupTask : IStartUpTask
    {
        private readonly IEnumerable<IMappingSetupTask> _tasks;

        public RunMappingSetupTask(IEnumerable<IMappingSetupTask> tasks)
        {
            _tasks = tasks;
        }

        public void Run()
        {
            if(_tasks == null) return;

            Mapper.Initialize(cfg =>
            {
                foreach (var mappingSetupTask in _tasks)
                {
                    mappingSetupTask.Run(cfg);
                }
            });
        }
    }
}