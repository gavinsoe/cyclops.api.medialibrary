﻿namespace Cyclops.Api.MediaLibrary.Infrastructure.Mappers
{
    public interface IMapper
    {
        TOutput Map<TOutput>(object input);
    }
}