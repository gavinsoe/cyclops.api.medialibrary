﻿using AutoMapper;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Mappers
{
    public interface IMappingSetupTask
    {
        void Run(IMapperConfigurationExpression cfg);
    }
}