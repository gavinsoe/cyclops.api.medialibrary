namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    public static class BoolExtensions
    {
        public static string ToYesNo(this bool? source, string nullAlt = null)
        {
            return source.HasValue ? source.Value ? "Yes" : "No" : nullAlt;
        }

        public static bool NullSafe(this bool? source)
        {
            return source ?? false;
        }
    }
}