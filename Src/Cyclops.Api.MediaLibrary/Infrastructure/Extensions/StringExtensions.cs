using System;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string source)
        {
            return !string.IsNullOrWhiteSpace(source);
        }

        public static string FormatWith(this string source, params object[] args)
        {
            return string.Format(source, args);
        }

        public static bool IsSame(this string source, string value)
        {
            return string.Equals(source, value, StringComparison.OrdinalIgnoreCase);
        }

        public static int? ToInt(this string source)
        {
            int result;
            return int.TryParse(source, out result) ?  result : (int?)null;
        }

        public static string EmptyAlt(this string source, string alt)
        {
            return string.IsNullOrWhiteSpace(source) ? alt : source;
        }
    }
}