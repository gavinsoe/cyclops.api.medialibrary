﻿using System;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    public static class UriExtensions
    {
        public static string BaseUrl(this Uri uri)
        {
            if (uri == null) return string.Empty;

            return $"{uri.Scheme}://{uri.Host}";
        }
    }
}