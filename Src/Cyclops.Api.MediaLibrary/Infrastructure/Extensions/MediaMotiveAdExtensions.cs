namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    public static class MediaMotiveAdExtensions
    {
        public static string ToAdValue(this string source)
        {
            return source?.ToLower().Replace(" ", string.Empty);
        }
    }
}