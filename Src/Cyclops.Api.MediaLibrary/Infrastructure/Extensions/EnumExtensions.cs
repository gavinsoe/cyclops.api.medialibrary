using System;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static TEnum? ToEnum<TEnum>(this string source) where TEnum : struct
        {
            if (string.IsNullOrWhiteSpace(source)) return default(TEnum);

            TEnum result;
            return Enum.TryParse(source, out result) ? result : default(TEnum?);
        }
    }
}