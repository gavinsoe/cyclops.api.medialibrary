using System.Collections.Generic;
using System.Linq;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class NullSafeExtensions
    {
        public static IEnumerable<T> NullSafe<T>(this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }
    }
}