﻿namespace Cyclops.Api.MediaLibrary.Infrastructure.StartUpTasks
{
    public interface IStartUpTask
    {
        void Run();
    }
}
