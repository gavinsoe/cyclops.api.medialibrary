﻿using System;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoBindAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AutoBindSelfAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AutoBindAsPerRequestAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class AutoBindAsSingletonAttribute : Attribute
    {
    }
}
