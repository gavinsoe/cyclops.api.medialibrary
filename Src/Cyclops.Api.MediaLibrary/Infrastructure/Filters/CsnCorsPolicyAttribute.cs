﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;
using Cyclops.Api.MediaLibrary.Infrastructure.Extensions;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Filters
{
    public class CsnCorsPolicyAttribute : Attribute, ICorsPolicyProvider
    {
        private readonly CorsPolicy _policy;

        public CsnCorsPolicyAttribute()
        {
            _policy = BuildPolicy();
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }

        private const string SettingsCorsAllowedOriginsName = "CorsAllowedOrigins";
        private CorsPolicy BuildPolicy()
        {
            var policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            var allowedDomains = System.Configuration.ConfigurationManager.AppSettings[SettingsCorsAllowedOriginsName]?.Split(',');

            if (allowedDomains.IsNullOrEmpty())
            {
                policy.AllowAnyOrigin = true;
            }
            else
            {
                foreach (var allowedDomain in allowedDomains)
                {
                    if (allowedDomain.IsSame("*"))
                    {
                        policy.Origins.Clear();
                        policy.AllowAnyOrigin = true;
                        break;
                    }
                    policy.Origins.Add(allowedDomain);
                }
            }

            return policy;
        }
    }

    public class CorsPolicyFactory : ICorsPolicyProviderFactory
    {
        readonly ICorsPolicyProvider _provider = new CsnCorsPolicyAttribute();

        public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return _provider;
        }
    }
}