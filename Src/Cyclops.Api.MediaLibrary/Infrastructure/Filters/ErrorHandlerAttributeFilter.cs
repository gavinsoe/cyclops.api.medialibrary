﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;
using Csn.Logging;

namespace Cyclops.Api.MediaLibrary.Infrastructure.Filters
{
    public class GlobalErrorHandlerAttributeFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var logger = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogger)) as ILogger;
            logger.Log(LogType.Error, filterContext.Exception.Message, filterContext.Exception);
        }

        public Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            var logger = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(ILogger)) as ILogger;
            logger.Log(LogType.Error, actionExecutedContext.Exception.Message, actionExecutedContext.Exception);
            return Task.FromResult(0);
        }
    }
}