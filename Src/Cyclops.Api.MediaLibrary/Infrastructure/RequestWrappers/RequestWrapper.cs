﻿using System;
using System.Web;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Infrastructure.Extensions;

namespace Cyclops.Api.MediaLibrary.Infrastructure.RequestWrappers
{
    public interface IRequestWrapper
    {
        Uri Uri { get; }
        string CorrelationId { get; }
        string Origin { get; }
    }

    [AutoBind]
    public class RequestWrapper : IRequestWrapper
    {
        public Uri Uri => HttpContext.Current?.Request.Url;

        private const string HeaderKey = "x-cid";
        public string CorrelationId
        {
            get
            {
                var httpContext = HttpContext.Current;

                if (httpContext == null) return Guid.NewGuid().ToShortId();

                if (httpContext.Items.Contains(HeaderKey))
                {
                    return httpContext.Items[HeaderKey].ToString();
                }
                
                var request = httpContext.Request;
                
                var cid = request.Headers[HeaderKey];

                if (cid.HasValue())
                {
                    httpContext.Items[HeaderKey] = cid;
                    return cid;
                }

                cid = Guid.NewGuid().ToShortId();
                
                httpContext.Items[HeaderKey] = cid;

                return cid;
            }
        }

        private const string OriginHeader = "x-appid";
        public string Origin => HttpContext.Current?.Request.Headers[OriginHeader];
    }
}