﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Cyclops.Api.MediaLibrary.Infrastructure.HealthChecks
{
    public interface IHealthChecker
    {
        Task<IHealthCheckResult> CheckAsync();
    }

    
}