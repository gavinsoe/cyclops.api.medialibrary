﻿using System.Collections.Generic;

namespace Cyclops.Api.MediaLibrary.Infrastructure.HealthChecks
{
    public class HealthCheckSummary
    {
        public bool IsHealthy { get; set; }
        public IEnumerable<IHealthCheckResult> Results { get; set; }
    }
}