using System;
using System.Linq;
using Autofac;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;

namespace Cyclops.Api.MediaLibrary.Ioc
{
    public class ConventionsModule : Module
    {
        private const string AssemblyStartsWithMatch = "Cyclops.Api.";

        protected override void Load(ContainerBuilder builder)
        {
            var assembliesToScan = AppDomain.CurrentDomain.GetAssemblies()
                .Where(a => a.FullName.StartsWith(AssemblyStartsWithMatch)).ToArray();

            builder.RegisterAssemblyTypes(assembliesToScan)
                .Where(t => t.CustomAttributes.Any(x => x.AttributeType == typeof(AutoBindSelfAttribute)))
                .AsSelf();

            builder.RegisterAssemblyTypes(assembliesToScan)
                .Where(t => t.CustomAttributes.Any(x => x.AttributeType == typeof(AutoBindAsSingletonAttribute)))
                .AsImplementedInterfaces()
                .SingleInstance();

            builder.RegisterAssemblyTypes(assembliesToScan)
                .Where(t => t.CustomAttributes.Any(x => x.AttributeType == typeof(AutoBindAttribute)))
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(assembliesToScan)
                .Where(t => t.CustomAttributes.Any(x => x.AttributeType == typeof(AutoBindAsPerRequestAttribute)))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }
    }
}