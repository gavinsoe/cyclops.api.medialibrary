﻿using Autofac;
using Csn.Cars.Cache.Builder;
using Csn.Logging;
using Cyclops.Api.MediaLibrary.Infrastructure.Mappers;
using ILogger = Csn.Logging.ILogger;

namespace Cyclops.Api.MediaLibrary.Ioc
{
    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutoMappedMapper>().As<IMapper>().SingleInstance();
            builder.Register(x => GetLogger.For<WebApiApplication>()).As<ILogger>().SingleInstance();
            builder.Register(x => CacheStoreBuilder.New().Build()).As<Csn.Cars.Cache.ICacheStore>().SingleInstance();
        }
    }
}