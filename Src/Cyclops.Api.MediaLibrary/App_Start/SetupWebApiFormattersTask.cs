﻿using System.Web.Http;
using Cyclops.Api.MediaLibrary.Infrastructure.Attributes;
using Cyclops.Api.MediaLibrary.Infrastructure.StartUpTasks;
using Newtonsoft.Json;

namespace Cyclops.Api.MediaLibrary.App_Start
{
    [AutoBind]
    public class SetupWebApiFormattersTask : IStartUpTask
    {
        public void Run()
        {
            var formatters = GlobalConfiguration.Configuration.Formatters;

            formatters.Remove(formatters.XmlFormatter);
            formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                //NullValueHandling = NullValueHandling.Ignore,
                //ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }
    }
}