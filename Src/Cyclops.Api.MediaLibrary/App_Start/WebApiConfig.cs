﻿using System.Net.Http.Extensions.Compression.Core.Compressors;
using System.Web.Http;
using Cyclops.Api.MediaLibrary.Infrastructure.Filters;
using Microsoft.AspNet.WebApi.Extensions.Compression.Server;

namespace Cyclops.Api.MediaLibrary
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.SetCorsPolicyProviderFactory(new CorsPolicyFactory());
            config.EnableCors();

            config.Filters.Add(new GlobalErrorHandlerAttributeFilter());

            config.MessageHandlers.Insert(0, new ServerCompressionHandler(new GZipCompressor(), new DeflateCompressor()));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "v1/{vertical}/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
